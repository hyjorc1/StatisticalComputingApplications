######################################################################
# Unit 6. Nonparametric Smoothing Method
# Lecture 2. Nonparametric Regression
######################################################################

######################################################################
#	Example 1. Motorcycle Crash Example
moto=read.table("Motorcycle.txt",head=TRUE)
plot(moto,xlab="time",ylab="head accelaration",main="Scatter Plot")
attach(moto)

library(KernSmooth)
# NW estimattion
p=0 # local linear fit
h=2 # bandwidth
m1=locpoly(x,y, bandwidth=h,degree=p)
plot(x,y,pch=20,xlab="time",ylab="acceleration")
lines(m1,col=2,lwd=2)
title("N-W Estimator (h=2)")

# local linear smoothing
p=1 # local linear fit
h=2 # bandwidth
m2=locpoly(x,y, bandwidth=h,degree=p)
lines(m2,col=4,lwd=2)
title("Local linear fit (h=2)")

# local linear smoothing with different bandwidth
plot(x,y,pch=20,xlab="time",ylab="acceleration")
m3=locpoly(x,y, bandwidth=0.5,degree=p)
lines(m3,col=2,lwd=2)
m4=locpoly(x,y, bandwidth=1,degree=p)
lines(m4,col=3,lwd=2)
m5=locpoly(x,y, bandwidth=2,degree=p)
lines(m5,col=4,lwd=2)
m6=locpoly(x,y, bandwidth=4,degree=p)
lines(m6,col=6,lwd=2)

# Local linear fit with direct plug-in bandwidth
h=dpill(x,y)
m7=locpoly(x,y, bandwidth=h,degree=p)
plot(x,y,pch=20,xlab="time",ylab="acceleration")
lines(m7,col=2,lwd=2)
title("Local linear fit with direct plug-in bandwidth")