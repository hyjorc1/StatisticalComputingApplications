#Problem 1

placebo=c(9243,9671,11792,13357,9055,6290,12412,18806)
oldpatch=c(17649,12013,19979,21826,13850,9806,17208,29044)
newpatch=c(16449,14614,17274,23798,12560,10157,16570,26325)
blood=cbind.data.frame(placebo,oldpatch,newpatch)
theta=abs(newpatch-oldpatch)/(oldpatch-placebo)
mean(theta)<=0.2

B=1000
theta.boot=rep(0,B)

for(i in 1:B){
  placebo.sam=sample(blood[,"placebo"], replace=T)
  old.sam=sample(blood[,"oldpatch"], replace=T)
  new.sam=sample(blood[,"newpatch"], replace=T)
  theta.boot[i]=abs(new.sam-old.sam)/(old.sam-placebo.sam)
}

blood.var=var(theta.boot)

theta.CI_low=quantile(theta.boot, 0.025)
theta.CI_up=quantile(theta.boot, 0.975)
theta.CI_low
theta.CI_up

#The confidence interval contains zero ([-5.279148,22.04124]) and so old and new are not significantly different at the .05 level.
#However, the mean bootstrapped theta=2.140272, which is above the FDA acceptable threshold.




#Problem 2

score=read.csv("C:\\Users\\psych_user\\Desktop\\score.csv")
score[,"pass"]=(score[,"score"]-score[,"cutoff"])>=0

year1=subset(score,year==1)
year2=subset(score,year==2)
year3=subset(score,year==3)
year4=subset(score,year==4)
year5=subset(score,year==5)

y1schA=subset(year1,school=="A")
y2schA=subset(year2,school=="A")
y3schA=subset(year3,school=="A")
y4schA=subset(year4,school=="A")
y5schA=subset(year5,school=="A")

mean(y1schA[,"pass"])
mean(y2schA[,"pass"])
mean(y3schA[,"pass"])
mean(y4schA[,"pass"])
mean(y5schA[,"pass"])

y1schB=subset(year1,school=="B")
y2schB=subset(year2,school=="B")
y3schB=subset(year3,school=="B")
y4schB=subset(year4,school=="B")
y5schB=subset(year5,school=="B")

mean(y1schB[,"pass"])
mean(y2schB[,"pass"])
mean(y3schB[,"pass"])
mean(y4schB[,"pass"])
mean(y5schB[,"pass"])

schoolA=subset(score,school=="A")
schoolB=subset(score,school=="B")
nrow(schoolA)
nrow(schoolB)
(nrow(schoolA)+nrow(schoolB))==nrow(score)

lr1=glm(pass~cutoff+school+cutoff:school,family=binomial,data=score)
lr2=glm(pass~cutoff+cutoff:school,family=binomial,data=score)
lr3=glm(pass~cutoff+school,family=binomial,data=score)
lr4=glm(pass~cutoff,family=binomial,data=score)
lr5=glm(pass~1,family=binomial,data=score)

summary(lr1)
summary(lr2)
summary(lr3)
summary(lr4)
summary(lr5)

#.65=4.979088-0.009060*cutoff+1.449129*schoolA-0.001760*schoolA*cutoff
#.65-4.979088=-0.009060*cutoff+1.449129*0-0.001760*0*cutoff
#.65-4.979088=-0.009060*cutoff
cutoff.65.A=(.65-4.979088)/-0.009060
cutoff.65.A

#.65=4.979088-0.009060*cutoff+1.449129*schoolB-0.001760*schoolB*cutoff
#.65-4.979088=-0.009060*cutoff+1.449129*1-0.001760*1*cutoff
#.65-4.979088=-0.009060*cutoff+1.449129-0.001760*cutoff
#.65-4.979088-1.449129=-0.009060*cutoff-0.001760*cutoff
#.65-4.979088-1.449129=(-0.009060-0.001760)*cutoff
cutoff.65.B=(.65-4.979088-1.449129)/(-0.009060-0.001760)
cutoff.65.B

AIC(lr1)
AIC(lr2)
AIC(lr3)
AIC(lr4)
AIC(lr5)

AIC(lr1,k=log(2542))
AIC(lr2,k=log(2542))
AIC(lr3,k=log(2542))
AIC(lr4,k=log(2542))
AIC(lr5,k=log(2542))

#AIC indicates model 1 is best, barely (model 3 is very close). BIC indicates that model 3 is best.

pass.pred1=rep(0,2542)
pass.pred2=rep(0,2542)
pass.pred3=rep(0,2542)
pass.pred4=rep(0,2542)
pass.pred5=rep(0,2542)

predict1=rep(0,nrow(score))
for(i in 1:nrow(score)){
  lr1=glm(pass~cutoff+school+cutoff:school,family=binomial,data=score[-i,])
  prob=predict(lr1,score[i,],type="response")
  ifelse(prob>0.5,pass.pred1<-1,pass.pred1<-0)
  ifelse(pass.pred1==score[i,"pass"],predict1[i]<-0,predict1[i]<-1)
}

predict2=rep(0,nrow(score))
for(i in 1:nrow(score)){
  lr2=glm(pass~cutoff+cutoff:school,family=binomial,data=score[-i,])
  prob=predict(lr2,score[i,],type="response")
  ifelse(prob>0.5,pass.pred2<-1,pass.pred2<-0)
  ifelse(pass.pred2==score[i,"pass"],predict2[i]<-0,predict2[i]<-1)
}

predict3=rep(0,nrow(score))
for(i in 1:nrow(score)){
  lr3=glm(pass~cutoff+school,family=binomial,data=score[-i,])
  prob=predict(lr3,score[i,],type="response")
  ifelse(prob>0.5,pass.pred3<-1,pass.pred3<-0)
  ifelse(pass.pred3==score[i,"pass"],predict3[i]<-0,predict3[i]<-1)
}

predict4=rep(0,nrow(score))
for(i in 1:nrow(score)){
  lr4=glm(pass~cutoff,family=binomial,data=score[-i,])
  prob=predict(lr4,score[i,],type="response")
  ifelse(prob>0.5,pass.pred4<-1,pass.pred4<-0)
  ifelse(pass.pred4==score[i,"pass"],predict4[i]<-0,predict4[i]<-1)
}

predict5=rep(0,nrow(score))
for(i in 1:nrow(score)){
  lr5=glm(pass~1,family=binomial,data=score[-i,])
  prob=predict(lr5,score[i,],type="response")
  ifelse(prob>0.5,pass.pred5<-1,pass.pred5<-0)
  ifelse(pass.pred5==score[i,"pass"],predict5[i]<-0,predict5[i]<-1)
}

mean(predict1)
mean(predict2)
mean(predict3)
mean(predict4)
mean(predict5)

library(boot)
set.seed(3)

#still need to do 5-fold CV


#Problem 3

soh=read.csv("C:\\Users\\psych_user\\Desktop\\soh.csv")